<?php
include ".htdbconfig.php";
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title>대여 신청</title>
    </head>
    <body>
        <h1>대여 신청</h1>
        <form method="POST" action="rent.php">
            <table>
                <thead>
                    <tr>
                        <th>대여 지점</th>
                        <td>
                            <select name="from">
                                <option>-- 선택하세요 --</option><?php
$result = $conn->query("SELECT seq, store_name FROM acid_store");
if($result)
{
    while($row = $result->fetch_assoc())
    {
        echo "
                                <option value=\"$row[seq]\">" . htmlspecialchars($row['store_name']) . "</option>";
    }
    $result->close();
}
?>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>반납 지점</th>
                        <td>
                            <select name="to">
                                <option>-- 선택하세요 --</option><?php
$result = $conn->query("SELECT seq, store_name FROM acid_store");
if($result)
{
    while($row = $result->fetch_assoc())
    {
        echo "
                                <option value=\"$row[seq]\">" . htmlspecialchars($row['store_name']) . "</option>";
    }
    $result->close();
}
?>

                            </select>
                        </td>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="2"><input type="submit" value="대여 신청"></td>
                    </tr>
                </tfoot>
                <tbody><?php
$result = $conn->query("SELECT seq, item_name FROM acid_items");
if($result)
{
    while($row = $result->fetch_assoc())
    {
        echo "
                    <tr>
                        <td>" . htmlspecialchars($row['item_name']) . "</td>
                        <td><input name=\"item_$row[seq]\" type=\"number\" value=\"0\" />개</td>
                    </tr>";
    }
    $result->close();
}
?>

                </tbody>
            </table>
        </form>
    </body>
</html>