<?php
include ".htdbconfig.php";
if(!$result = $conn->query("SELECT acid_log.seq s, a.store_name s1, b.store_name s2, start, end FROM acid_log INNER JOIN acid_store a ON a.seq = store_from INNER JOIN acid_store b ON b.seq = store_to WHERE end IS NOT NULL ORDER BY end DESC"))
{
    $conn->close();
    exit("Failed!");
}
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title>대여 기록</title>
    </head>
    <body>
        <h1>대여 기록</h1>
        <table>
            <thead>
                <tr>
                    <th>대여 지점</th>
                    <th>반납 지점</th>
                    <th>대여일</th>
                    <th>반납일</th>
                    <th>ID</th>
                </tr>
            </thead>
            <tbody><?php
if($row = $result->fetch_assoc())
{
    do
    {
        echo "
                <tr>
                    <td>" . htmlspecialchars($row['s1']) . "</td>
                    <td>" . htmlspecialchars($row['s2']) . "</td>
                    <td>" . htmlspecialchars($row['start']) . "</td>
                    <td>" . htmlspecialchars($row['end']) . "</td>
                    <td><a href=\"status.php?id=$row[s]\">$row[s]</a></td>
                </tr>";
    }
    while($row = $result->fetch_assoc());
}
else
{
    echo "
                <tr>
                    <td colspan=\"5\">아직 반납까지 마친 대여가 없습니다!</td>
                </tr>";
}
$result->close();
$conn->close();
?>

            </tbody>
        </table>
    </body>
</html>