<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>ACID Example</title>
    </head>
    <body>
        <iframe src="store.php?id=1"></iframe>
        <iframe src="store.php?id=2"></iframe>
        <iframe src="store.php?id=3"></iframe>
        <hr />
        <iframe src="item.php?id=1"></iframe>
        <iframe src="item.php?id=2"></iframe>
        <iframe src="item.php?id=3"></iframe>
        <iframe src="item.php?id=4"></iframe>
        <hr />
        <a href="rent_form.php">대여하기</a>
        <iframe src="status.php"></iframe>
        <iframe src="log.php"></iframe>
    </body>
</html>