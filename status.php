<?php
include ".htdbconfig.php";
$id = 0;
if(isset($_GET['id']))
{
    $id = intval($_GET['id']);
}
if(!$id)
{
    if(!$result = $conn->query("SELECT acid_log.seq s, a.store_name s1, b.store_name s2, start FROM acid_log INNER JOIN acid_store a ON a.seq = store_from INNER JOIN acid_store b ON b.seq = store_to WHERE end IS NULL ORDER BY acid_log.seq DESC"))
    {
        $conn->close();
        exit("Failed!");
    }
    ?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title>대여 현황</title>
    </head>
    <body>
        <h1>대여 현황</h1>
        <table>
            <thead>
                <tr>
                    <th>대여 지점</th>
                    <th>반납 지점</th>
                    <th>대여일</th>
                    <th>ID</th>
                </tr>
            </thead>
            <tbody><?php
    if($row = $result->fetch_assoc())
    {
        do
        {
            echo "
                <tr>
                    <td>" . htmlspecialchars($row['s1']) . "</td>
                    <td>" . htmlspecialchars($row['s2']) . "</td>
                    <td>" . htmlspecialchars($row['start']) . "</td>
                    <td><a href=\"status.php?id=$row[s]\">$row[s]</a></td>
                </tr>";
        }
        while($row = $result->fetch_assoc());
    }
    else
    {
        echo "
                <tr>
                    <td colspan=\"4\">아직 반납하지 않은 대여가 없습니다!</td>
                </tr>";
    }
    $result->close();
    $conn->close();
    ?>

            </tbody>
        </table>
    </body>
</html><?php
    exit();
}
$result = $conn->query("SELECT a.store_name s1, b.store_name s2, start, end FROM acid_log INNER JOIN acid_store a ON a.seq = store_from INNER JOIN acid_store b ON b.seq = store_to WHERE acid_log.seq = $id");
if(!$result)
{
    $conn->close();
    exit("Failed!");
}
if(!$row = $result->fetch_assoc())
{
    $conn->close();
    exit("Failed!");
}
$result->close();
$result = $conn->query("SELECT item, cnt FROM acid_item INNER JOIN acid_log ON acid_log.seq = acid_item.seq WHERE acid_log.seq = $id");
if(!$result)
{
    $conn->close();
    exit("Failed!");
}
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title>대여 현황 - <?=$id?></title>
    </head>
    <body>
        <h1>대여 현황 - <?=$id?></h1>
        <table>
            <tbody>
                <tr>
                    <th>대여 지점</th>
                    <td><?=htmlspecialchars($row['s1'])?></td>
                </tr>
                <tr>
                    <th>반납 지점</th>
                    <td><?=htmlspecialchars($row['s2'])?></td>
                </tr>
                <tr>
                    <th>대여일</th>
                    <td><?=$row['start']?></td>
                </tr>
                <tr>
                    <th>반납일</th><?php
echo $row['end'] ? "
                    <td>$row[start]</td>" : "
                    <td><a href=\"return.php?id=$id\">반납</a></td>";
?>

            </tbody>
        </table>
        <table>
            <thead>
                <tr>
                    <th>품목</th>
                    <th>수량</th>
                </tr>
            </thead><?php
if(!$row['end'])
{
    echo "
            <tfoot>
                <tr>
                    <td colspan=\"2\"><a href=\"return.php?id=$id\">반납</a></td>
                </tr>
            </tfoot>";
}
?>

            <tbody><?php
while($row = $result->fetch_assoc())
{
    echo "
                <tr>
                    <td>" . htmlspecialchars($row['item']) . "</td>
                    <td>$row[cnt]</td>
                </tr>";
}
$result->close();
$conn->close();
?>

            </tbody>
        </table>
    </body>
</html>