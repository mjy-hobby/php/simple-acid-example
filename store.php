<?php
$store = 0;
if(isset($_GET['id']))
{
    $store = intval($_GET['id']);
}
if(!$store)
{
    echo "조회 실패!";
    exit();
}
include ".htdbconfig.php";
$result = $conn->query("SELECT store_name FROM acid_store WHERE seq = $store");
if(!$result)
{
    $conn->close();
    exit("Failed!");
}
$row = $result->fetch_assoc();
if(!$row)
{
    $conn->close();
    exit("없는 지점입니다!");
}
$name = $row['store_name'];
$result->close();
$result = $conn->query("SELECT item_name, cnt FROM acid_stock INNER JOIN acid_items WHERE store = $store AND seq = item");
if(!$result)
{
    $conn->close();
    exit("Failed!");
}
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?=htmlspecialchars($name)?> 재고</title>
    </head>
    <body>
        <h1><?=htmlspecialchars($name)?> 재고</h1>
        <table>
            <thead>
                <tr>
                    <th>품목</th>
                    <th>재고</th>
                </tr>
            </thead>
            <tbody><?php
if($row = $result->fetch_assoc())
{
    do
    {
        echo "
                <tr>
                    <td>" . htmlspecialchars($row['item_name']) . "</td>
                    <td>$row[cnt]개</td>
                </tr>";
    }
    while($row = $result->fetch_assoc());
}
else
{
    echo "
                <tr>
                    <td colspan=\"2\">이 지점이 취급하는 품목이 아직 없습니다!</td>
                </tr>";
}
$result->close();
$conn->close();
?>

            </tbody>
        </table>
    </body>
</html>